FROM golang:1.11-alpine

WORKDIR /go/src/app

COPY . .

CMD [ "go", "run", "cmd/service/main.go" ]

EXPOSE 8080
