package proxy

type Config struct {
	Sites []Site `yaml:"sites"`
}

type Site struct {
	Path        string `yaml:"path"`
	Port        int    `yaml:"port"`
	Service     string `yaml:"service"`
	Healthcheck string `yaml:"healthcheck"`
}
