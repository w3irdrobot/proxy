FROM golang:1.11-alpine

WORKDIR /go/src/app

COPY . .

CMD [ "go", "run", "cmd/proxy/main.go" ]

EXPOSE 8080
